tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
import tb.antlr.symbolTable.LocalSymbols;
}

@members {
LocalSymbols memory = new LocalSymbols();
}

prog    : (
        ^(VAR i1=ID) {memory.newSymbol($i1.text);}
        | ^(PODST i1=ID e2=expr) {memory.setSymbol($i1.text, $e2.out);}
        | ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());}
        | LCB {memory.enterScope();}
        | RCB {memory.leaveScope();}
        )*
        ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(MOD   e1=expr e2=expr) {$out = $e1.out \% $e2.out;}
        | ^(ABS   e1=expr)         {$out = Math.abs($e1.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = memory.getSymbol($ID.text);}
        ;
