grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

/*
wykonane zadanie:
 - zaprogramowanie generowania kodu w architekturze stosowej dla czterech podstawowych operacji,
 - dodanie obslugi zmiennych globalnych,
 - zaimplementowanie funkcjonalnosci pokazanych w materialach yt (dodanie blokow, instrukcja if),
 - zaprogramowanie petli while i do while opartych o wyrazenia arytmetyczne.
*/

prog
    : (stat | blok)+ EOF!;
    
blok : LCB^ (stat | blok)* RCB!
;

stat
    : expr NL -> expr

    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
//    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr NL -> ^(PRINT expr)
    | if_stat NL -> if_stat
    | while_stat NL -> while_stat
    | do_while_stat NL -> do_while_stat
//    | LCB NL -> LCB
//    | RCB NL -> RCB
    | NL ->
    ;
    
if_stat 
    : IF^ expr THEN! (expr) (ELSE! (expr))?
;

while_stat
    : WHILE^ expr DO! blok
    ;
    
do_while_stat
    : DO^ blok WHILE! expr
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | MOD^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    | ABS expr ABS -> ^(ABS expr)
    ;

VAR :'var';

PRINT : 'print';

IF : 'if';

THEN : 'then';

ELSE: 'else';

WHILE : 'while';

DO : 'do';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;
	
LCB : '{';

RCB : '}';
	
ABS : '|';

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
MOD : '%';
